cliptop = 12;
clipbottom = 0;

nRows = size(areas,1);
nColumns = size(areas,2);

areaClip = areas;

for col = 1:nColumns
    % Clip top end
    for row = 1:nRows
        if areas(row,col)>0
            break;
        end
    end
    topEnd = row;

    % Clip bottom end
    for row = nRows:-1:1
        if areas(row,col)>0
            break;
        end
    end
    bottomEnd = row;
    
    areaClip(cat(2,1:topEnd+cliptop,5:7),col) = 0;
end

volumes = sum(areaClip,1)*10e-6;

figure;
plot([1850,1800,1750,1700,1650,1600,1550,1500],volumes);
% muscleLengths = [1850,1800,1750,1700,1650,1600,1550,1500];
% plotyy(muscleLengths,volumes,muscleLengths,volumes/volumes(1));
xlabel('Muscle Length (\mum)','FontSize',12); ylabel({'Muscle Volume (m^3)'},'FontSize',12); 