function [ output_args ] = ExportPNGs( V, slices, frames, folderPath )
%EXPORTPNGS Summary of this function goes here
%   Detailed explanation goes here

for frame = 1:frames
    for slice = 1:slices
        imwrite(mat2gray(V(:,:,slices*(frame-1)+slice),[0 255]),[folderPath sprintf('\\volume_t%04d_z%04d.png',frame,slice)])
    end
end

