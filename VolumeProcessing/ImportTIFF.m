function [ X, Y, Z, V ] = ImportTIFF( folderPath, xDim, yDim, zDim )
%IMPORTPNGS Imports 3D volume data from PNG dataset
%   Detailed explanation goes here

    contents = dir(folderPath);

    % Count Images
    nImages = 0;    
    for i = 1:length(contents)
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'tif'))
            nImages = nImages+1;
        end
    end

    % Pre-initialize arrays
    image = imread(strcat(folderPath,'\',contents(5).name)); %Get size
    nRows = size(image,1);
    nColumns = size(image,2); 

    X = linspace(0,(nColumns-1)*xDim,nColumns);
    Y = linspace(0,(nRows-1)*yDim,nRows);
    Z = linspace(0,(nImages+2-1)*zDim,nImages);
    V = zeros(nRows,nColumns,nImages); 
   
    %Z = linspace(0,(nImages+2-1)*zDim,nImages+2);
    %V = zeros(nRows,nColumns,nImages+2);   %Pad top and bottom with zeros

    count = 0;

    % Iterate all files
    for i = 1:length(contents)
        % Only deal with images
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'tif'))
            array = imread(strcat(folderPath,'\',contents(i).name));
            count = count+1; % increment
            V(:,:,count) = array;
        end
    end

end

