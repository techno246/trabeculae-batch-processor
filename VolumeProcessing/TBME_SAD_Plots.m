%% PARAMETERS
imageDirectory = 'F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20151210 Pure OCT\Stationary Single Section 2\Cropped';
repeats = 100;
frames = 1;
resampling_factor = 1;
x_dim = 2.7e-6/resampling_factor;
y_dim = 10e-6/resampling_factor;
z_dim = 10e-6/resampling_factor;
FORCE_PRESENT = true;
stimFrequency = 2;
batchsize = 50;

close all;

%% FIND SAD
contents = dir(imageDirectory);

% Count Images
nImages = 0;    
for i = 1:length(contents)
    if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
        nImages = nImages+1;
    end
end

% Pre-initialize arrays
% Note that dimensions are funky because iso2mesh processes arrays
% where dimension 1 is x (rows)

imagePath = cell(frames,repeats);
sad = zeros(frames,repeats);
sad_out = zeros(frames);
sad_average = zeros(frames,repeats/batchsize);

count = 0;

% Iterate all files
for i = 1:length(contents)
    % Only deal with images
    if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
        frame = mod(count,frames)+1;
        repeat = floor(count/frames)+1;
        imagePath{frame,repeat} = strcat(imageDirectory,'\',contents(i).name);
        count = count+1; % increment
    end
end
baseImage = imread(imagePath{1,1})
nRows = size(baseImage,1);
nColumns = size(baseImage,2); 


% % Sum of absolute on raw
% for frame = 1:frames
%     for n = 1:repeats
%         original = imread(imagePath{frame,1});
%         next = imread(imagePath{frame,n});
%         sad(frame,n) = sum(sum(imabsdiff(original,next)));
%         disp(strcat('Frame ',int2str(frame),' N ',int2str(n)))
%     end
% end
% 
% % Sum of absolute on different frames
% for frame = 1:frames
%     original = imread(imagePath{1,1});
%     next = imread(imagePath{frame,1});
%     sad_out(frame) = sum(sum(imabsdiff(original,next)));
%     disp(strcat('Frame ',int2str(frame),' N ',int2str(n)))
% end


% Sum of absolute on averaged data
if ~exist('averagedImages', 'var')
    sumIntensities = zeros(nRows,nColumns);
    averagedImages = cell(frames,repeats/batchsize);
    for frame = 1:frames
        averagedCount = 0;
        for n = 1:repeats
            %original = imread(imagePath{frame,1});
            %next = imread(imagePath{frame,n});
            batch = mod(n-1,batchsize)+1;
            if (batch == batchsize) 
                averagedCount = averagedCount+1;
                averagedImages{frame,averagedCount} = sumIntensities./batchsize;
                sumIntensities = zeros(nRows,nColumns);
            else
                sumIntensities = sumIntensities+double(imread(imagePath{frame,n}));
            end

            disp(strcat('Frame ',int2str(frame),' N ',int2str(n)))
        end
    end
end


for frame = 1:frames
    for n = 1:repeats/batchsize
        diff(frame, n) = sum(sum(imabsdiff(averagedImages{frame,1},averagedImages{frame,n})));
    end
end

% Sum of absolute on different frames
for frame = 1:frames
    original = imread(imagePath{1,1});
    next = imread(imagePath{frame,1});
    sad_out(frame) = sum(sum(imabsdiff(averagedImages{1,1},averagedImages{frame,1})));
    disp(strcat('Frame ',int2str(frame),' N ',int2str(n)))
end

% Median cross sectional area 
for frame = 1:frames
    original = imread(imagePath{1,1});
    next = imread(imagePath{frame,1});
    sad_out(frame) = sum(sum(imabsdiff(averagedImages{1,1},averagedImages{frame,1})));
    disp(strcat('Frame ',int2str(frame),' N ',int2str(n)))
end
