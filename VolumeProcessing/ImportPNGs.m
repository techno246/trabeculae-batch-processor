function [ X, Y, Z, V ] = ImportPNGs( folderPath, xDim, yDim, zDim )
%IMPORTPNGS Imports 3D volume data from PNG dataset
%   Detailed explanation goes here

    contents = dir(folderPath);

    % Count Images
    nImages = 0;    
    for i = 1:length(contents)
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
            nImages = nImages+1;
        end
    end

    % Pre-initialize arrays
    % Note that dimensions are funky because iso2mesh processes arrays
    % where dimension 1 is x (rows)
    image = imread(strcat(folderPath,'\',contents(5).name))'; %Get size
    nRows = size(image,1);
    nColumns = size(image,2); 
    
    X = linspace(0,(nRows-1)*xDim,nRows);
    Y = linspace(0,(nColumns-1)*yDim,nColumns);
    Z = linspace(0,(nImages-1)*zDim,nImages);
    V = zeros(nRows,nColumns,nImages); 
   
    %Z = linspace(0,(nImages+2-1)*zDim,nImages+2);
    %V = zeros(nRows,nColumns,nImages+2);   %Pad top and bottom with zeros

    count = 0;

    % Iterate all files
    for i = 1:length(contents)
        % Only deal with images
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
            array = imread(strcat(folderPath,'\',contents(i).name))';
            count = count+1; % increment
            V(:,:,count) = array;
        end
    end
    
    V = V;

end

