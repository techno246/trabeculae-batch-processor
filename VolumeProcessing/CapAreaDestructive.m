function [ area ] = CapArea( area, capAmount )
%CAPAREA Same as cap area except it's replaced with NAN instead of
%neighbouring values.
%   Detailed explanation goes here

    pos = 1;
    % Upwards
    while area(pos) == 0 || isnan(area(pos))
        pos = pos+1;
        if pos > length(area)
            break
        end
    end
    if pos <= length(area)
        area(pos:pos+capAmount) = NaN;
    end

    %Downwards
    pos = length(area);

    while area(pos) == 0 || isnan(area(pos))
        pos = pos-1;
        if pos < 0
            break
        end
    end
    if pos >= 0 
        area(pos-capAmount:pos) = NaN;
    end
end

