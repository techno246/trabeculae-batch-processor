function [ stress ] = BatchStress( area , force )
%BATCHMESH3D Summary of this function goes here
%   Detailed explanation goes here

slices = size(area,2);

for y = 1:slices
    stress(:,y) = force./area(:,y);
end

end

