function [ maskList ] = BatchSliceSurface( nodeList, faceList )
%BATCHMESH3D Summary of this function goes here
%   Detailed explanation goes here


maskList = cell(frames,1);

for n = 1:frames
    node = nodeList{n};
    face = faceList{n};
    [node, face] = meshcheckrepair(node,face,'meshfix');
    [node,elem,face]=surf2mesh(node,face,[1 1 1],[100 175 284],1,25);
    [mask weight] = m2v(node(:,1:3), elem(:,1:4), [175,175,284]);
    maskList{n} = mask;
end

end

