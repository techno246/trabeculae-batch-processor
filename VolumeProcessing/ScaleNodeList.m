function [ nodeList ] = ScaleNodeList( nodeList,x_dim,y_dim,z_dim )
%SCALENODELIST Summary of this function goes here
%   Detailed explanation goes here
    for i = 1:length(nodeList)
        nodeList{i}(:,1) = nodeList{i}(:,1).*x_dim;
        nodeList{i}(:,2) = nodeList{i}(:,2).*y_dim;
        nodeList{i}(:,3) = nodeList{i}(:,3).*z_dim;
    end
end

