%% PARAMETERS
imageDirectory = {
    'F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20160717\17_05_FullContraction\PNGs\Segmented'
    };
frames = 100;
resampling_factor = 1;
x_dim = 2.7e-6/resampling_factor;
y_dim = 10e-6/resampling_factor;
z_dim = 10e-6;
FORCE_PRESENT = false;
stimFrequency = 1;

close all;

%% FIND AREA
if ~exist('volumes', 'var')
    volumes = zeros(frames,length(imageDirectory));
    areaHold = cell(length(imageDirectory),1);
            
    % Iterate all files
    for n = 1:length(imageDirectory)
        %% INTIALISE ARRAYS
        % Count Images
        contents = dir(imageDirectory{n});
        nImages = 0;    
        for i = 1:length(contents)
            if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
                nImages = nImages+1;
            end
        end

        % Calculate number of slices
        slices = nImages/frames;

        % Pre-initialize arrays
        % Note that dimensions are funky because iso2mesh processes arrays
        % where dimension 1 is x (rows)
        image = imread(strcat(imageDirectory{n},'\',contents(5).name))'; %Get size
        nRows = size(image,1);
        nColumns = size(image,2); 

        areas = zeros(slices,nImages/slices); 

        count = 0;

        %% CALCULATE AREAS
        for i = 1:length(contents)
            % Only deal with images
            if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
                array = imread(strcat(imageDirectory{n},'\',contents(i).name))';
                slice = mod(count,slices)+1;
                frame = floor(count/slices)+1;
                count = count+1; % increment

                % Do stuff
                areas(slice,frame) = sum(sum(logical(array))).*x_dim.*y_dim;
            end
        end
        
        volumes(:,n) = sum(areas,1).*z_dim;
        areaHold{n} = areas;
    end
end
% 
% %% PLOT FORCE AND STRESS
% figure;
% 
% if FORCE_PRESENT
%     % Plot force 
%     subplot (3,1,1);
%     plot(ForceData(:,1),(ForceData(:,2)-min(ForceData(:,2)))*1e3);
%     xlabel('Time (s)','FontSize',12); ylabel({'Active Force (mN)'},'FontSize',12);
%     
%     % Plot stress 
%     subplot (3,1,3);
%     force_new = ForceData(:,2);
%     force_new = force_new - min(force_new);
%     [stress] = BatchStress(volumes', force_new, stimFrequency);
%     caxis([0 22.5])
%     
%     % Setup plot for area
%     subplot (3,1,2);
% end
% 

%% PLOT VOLUMES

% Color map
smoothing = true;
colormap default;

time = 0:0.01:(frames-1)/100; %en mm
stretchN = 1:length(imageDirectory);

[TimeGrid, PosGrid] = meshgrid(stretchN, time);
%surf(TimeGrid', PosGrid', areaHold, 'LineStyle', 'none', 'FaceLighting', 'none', 'FaceColor', 'interp');
pcolor(TimeGrid', PosGrid', volumes');
%shading('interp');
%title(sprintf('Average cross-sectional area along muscle length during twitch'),'FontSize',14);
xlabel('Muscle Length (% L_O)','FontSize',12); ylabel('Time (s)','FontSize',12);
c = colorbar;
ylabel(c, 'Volume (m^3)','rot',-90,'FontSize',12);

% Volume
figure;
plot(time,volumes);
xlabel('Time (s)','FontSize',12); ylabel('Volume (m^3)','FontSize',12);

% Differences
figure;
diff = (max(volumes,[],1)-min(volumes,[],1))./mean(volumes,1);
scatter([100,95,90,85,80,75],diff.*100,'filled');
xlabel('Muscle Length (% L_O)','FontSize',12); ylabel('Volume Range (%)','FontSize',12);

% Area range
figure;
for n = 1:length(imageDirectory)
    midpoint = floor(size(areaHold{n},1)/2);
    bottom = midpoint - 60;
    top = midpoint + 60;
    diff = max(areaHold{n}(bottom:top,:),[],2)-min(areaHold{n}(bottom:top,:),[],2);
    diff = 100.*diff./mean(areaHold{n}(bottom:top,:),2);
    meandiff(n) = mean(diff);
end
plot(meandiff);

% Area variation
figure;
hold on;

for n = 1:length(imageDirectory)
    midpoint = floor(size(areaHold{n},1)/2);
    bottom = midpoint - 100;
    top = midpoint + 100;
    diff = mad(areaHold{n}(1:end,:),1,1)./mean(areaHold{n}(1:end,:),1).*100;
    plot(time,diff);
end
%scatter([100,95,90,85,80,75],meandiff,'filled');
%plot(meandiff);
xlabel('Time (s)','FontSize',12); ylabel('MAD of Areas (%)','FontSize',12);
legend('100 % L_O','95 % L_O','90 % L_O','85 % L_O','80 % L_O','75 % L_O');
hold off;

% Median area
figure;
for n = 1:length(imageDirectory)
    medianAreas(n) = median(areaHold{n}(:,1),1);
end
scatter([100,95,90,85,80,75],medianAreas,'filled');
xlabel('Muscle Length (% L_O)','FontSize',12); ylabel('Median Area (%)','FontSize',12);


% % MAD of Volume
% figure;
% meanabsolute = mad(volumes,1);
% meanvolumes = mean(volumes,1);
% scatter([100,95,90,85,80,75],(meanabsolute./meanvolumes).*100,'filled');
% xlabel('Muscle Length (% L_O)','FontSize',12); ylabel('MAD of Volume (mm)','FontSize',12);
% 
% % MAD of Areas
% figure;
% hold on;
% for n = 1:length(imageDirectory)
%     areas = areaHold{n};
%     meanabsolute = mad(areas,1);
%     meanvolumes = mean(areas,1);
%     plot((meanabsolute./meanvolumes).*100);
% end
% hold off;
% xlabel('Muscle Length (% L_O)','FontSize',12); ylabel('MAD of Area (mm)','FontSize',12);
%     
%% OTHER SHIT
% Plot force

% if ~FORCE_PRESENT
%     subplot (3,1,1);
%     plot(ForceData(:,1),(ForceData(:,2)-min(ForceData(:,2)))*1e3);
%     xlabel('Time (s)','FontSize',12); ylabel({'Active Force (mN)'},'FontSize',12);
%     c = colorbar;
% end
% 
% % Find area
% subplot (3,1,2);
% [areas change] = BatchArea( areas , slices, frames_new, 0, stimFrequency, x_dim, y_dim, z_dim, dec_factor);
% 
% % Find stress
% if ~FORCE_PRESENT
%     subplot (3,1,3);
%     force_new = decimate(ForceData(:,2), dec_factor);
%     force_new = force_new - min(force_new);
%     [stress] = BatchStress(areas,force_new,stimFrequency);
% end
% 
% % Volume Fixed
% 
% % lengthEnd = 0.00147-0.00067;
% % lengthStart = 0.00163-0.00055;
% % lengths = linspace(lengthStart,lengthEnd,6);
% % volume2 = nanmean(areas,2)';
% % volume2=volume2.*lengths;
% % figure;
% % plot ([100,95,90,85,80,75], volume2);
% % title('Volume of muscle during stretch','FontSize',14);
% % xlabel('Muscle Length (%L_O)','FontSize',12); ylabel('Volume (m^3)','FontSize',12);
% % 
% % % Find volume
% % for frame = 1:frames_new
% %     TRI = faceList{frame}(:,1:3);
% %     X_ = nodeList{frame}(:,1);
% %     Y_ = nodeList{frame}(:,2);
% %     Z_ = nodeList{frame}(:,3);
% %     [volume(frame),surfaceArea(frame)] = triangulationVolume(TRI,X_,Y_,Z_);
% %     
% % end
% 
% %AnimateMesh( nodeList, faceList, frames, rate, dec_factor )
% 
% if FORCE_PRESENT
%     [ nodeListC, faceListC, cRange ] = ColorWithCSArea(nodeList, faceList, areas, slices, frames_new,  x_dim, y_dim, z_dim );
% else
%     decimatedForce = decimate(ForceData(:,2),dec_factor);
%     [ nodeListC, faceListC, cRange ] = ColorWithStress(nodeList, faceList, areas, slices, frames_new,  x_dim, y_dim, z_dim, decimatedForce );
%     %cRange = [0 9.68415013613380e-12]
% end
% 
% movieMatrix = AnimateMesh( nodeListC, faceListC, frames_new, rate, dec_factor_new, max(X), max(Y), max(Z)/frames, cRange );
% 
% 
