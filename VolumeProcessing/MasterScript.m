imageDirectory = 'F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20160225 EMBC\Raw\Cropped\Segmented\New folder';
slices = 195;
frames = 1;
rate = 1;
dec_factor = 1;
x_dim = 2.7e-6;
y_dim = 10e-6;
z_dim = 10e-6;
COLOR_WITH_AREA = true;
stimFrequency = 2;

close all;

% cantilever m/count = 0.000000000309078
% cantilever N/m = 1400

if ~exist('nodeList')
    [X, Y, Z, V] = ImportPNGs(imageDirectory, x_dim, y_dim, z_dim);
    V=V;
    V = DecimateVolume(V,dec_factor,slices); 
    frames_new = frames/dec_factor; 
    dec_factor_new = 1;
    [ nodeList, faceList ] = BatchMesh3D( V , slices, frames_new );
    nodeList = ScaleNodeList(nodeList, x_dim, y_dim, z_dim);
end

figure;

% Plot force

if ~COLOR_WITH_AREA
    subplot (3,1,1);
    plot(ForceData(:,1),(ForceData(:,2)-min(ForceData(:,2)))*1e3);
    xlabel('Time (s)','FontSize',12); ylabel({'Active Force (mN)'},'FontSize',12);
    c = colorbar;
end

% Find area
subplot (3,1,2);
[areas change] = BatchArea( V , slices, frames_new, 0, stimFrequency, x_dim, y_dim, z_dim, dec_factor);

% Find stress
if ~COLOR_WITH_AREA
    subplot (3,1,3);
    force_new = decimate(ForceData(:,2), dec_factor);
    force_new = force_new - min(force_new);
    [stress] = BatchStress(areas,force_new,stimFrequency);
end

% Volume Fixed

% lengthEnd = 0.00147-0.00067;
% lengthStart = 0.00163-0.00055;
% lengths = linspace(lengthStart,lengthEnd,6);
% volume2 = nanmean(areas,2)';
% volume2=volume2.*lengths;
% figure;
% plot ([100,95,90,85,80,75], volume2);
% title('Volume of muscle during stretch','FontSize',14);
% xlabel('Muscle Length (%L_O)','FontSize',12); ylabel('Volume (m^3)','FontSize',12);
% 
% % Find volume
% for frame = 1:frames_new
%     TRI = faceList{frame}(:,1:3);
%     X_ = nodeList{frame}(:,1);
%     Y_ = nodeList{frame}(:,2);
%     Z_ = nodeList{frame}(:,3);
%     [volume(frame),surfaceArea(frame)] = triangulationVolume(TRI,X_,Y_,Z_);
%     
% end

%AnimateMesh( nodeList, faceList, frames, rate, dec_factor )

if COLOR_WITH_AREA
    [ nodeListC, faceListC, cRange ] = ColorWithCSArea(nodeList, faceList, V, slices, frames_new,  x_dim, y_dim, z_dim );
else
    decimatedForce = decimate(ForceData(:,2),dec_factor);
    [ nodeListC, faceListC, cRange ] = ColorWithStress(nodeList, faceList, V, slices, frames_new,  x_dim, y_dim, z_dim, decimatedForce );
    %cRange = [0 9.68415013613380e-12]
end

movieMatrix = AnimateMesh( nodeListC, faceListC, frames_new, rate, dec_factor_new, max(X), max(Y), max(Z)/frames, cRange );


