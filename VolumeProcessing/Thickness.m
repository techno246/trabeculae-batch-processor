%% PARAMETERS
imageDirectory = 'F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20160908\OCT\Segmented';
slices = 163;
frames = 1;
resampling_factor = 2;
refractive_index = 1.37; % That of human tissue
z_dim = 2.7e-6/refractive_index/resampling_factor;
y_dim = 10e-6/resampling_factor;
x_dim = 10e-6;
FORCE_PRESENT = false;
stimFrequency = 1;

close all;

%% FIND THICKNESSES

if ~exist('thicknessZ', 'var')
    %% INTIALISE ARRAYS
    % Count Images
    contents = dir(imageDirectory);
    nImages = 0;    
    for i = 1:slices*frames
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
            nImages = nImages+1;
        end
    end

    % Pre-initialize arrays
    % Note that dimensions are funky because iso2mesh processes arrays
    % where dimension 1 is x (rows)
    image = imread(strcat(imageDirectory,'\',contents(5).name))'; %Get size
    nRows = size(image,1);
    nColumns = size(image,2); 

    thicknessY = zeros(nRows, slices, frames);
    thicknessZ = zeros(nColumns, slices, frames);

    count = 0;

    %% CALCULATE AREAS
    % Iterate all files
    for i = 1:length(contents)
        % Only deal with images
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
            array = imread(strcat(imageDirectory,'\',contents(i).name))';
            slice = mod(count,slices)+1;
            frame = floor(count/slices)+1;
            count = count+1; % increment
            if count>slices*frames
                break
            end
            % Calculate thickness
            thicknessY(:,slice,frame) = (sum(logical(array),2).*y_dim.*1e3);
            thicknessZ(:,slice,frame) = (sum(logical(array),1).*z_dim.*1e3);
        end 
    end
end
    
%% PLOT Thicknesses
smoothing = true;
colormap default;

x = (1:slices)*x_dim*1e3; %en mm
z = (1:nRows)*z_dim*1e3; %en mm
y = (1:nColumns)*y_dim*1e3; %en mm

[xzGrid, zxGrid] = meshgrid(x, z);
[xyGrid, yxGrid] = meshgrid(x, y);

x_microscope = (1:slices)*x_dim/0.273e-6; %en mm
z_microscope = (1:nRows)*z_dim/0.273e-6; %en mm
[xzGrid_microscope, zxGrid_microscope] = meshgrid(x_microscope, z_microscope);


% Direction that corresponds with the Microscope
figure;
pcolor(xzGrid', zxGrid', thicknessY(:,:,1)');
shading('interp');
xlabel('X (mm)','FontSize',12); ylabel('Z (mm)','FontSize',12);
c = colorbar;
ylabel(c, 'Thickness (mm)','rot',-90,'FontSize',12);

% Direction that corresponds with the OCT imaging plane
figure;
pcolor(xyGrid', yxGrid', thicknessZ(:,:,1)');
shading('interp');
xlabel('X (mm)','FontSize',12); ylabel('Y (mm)','FontSize',12);
c = colorbar;
ylabel(c, 'Thickness (mm)','rot',-90,'FontSize',12);

% Export as 32 bit image to find mapping parameters. 
Vx = 1*x_dim*1e3:273e-6:slices*x_dim*1e3;
Vz = 1*z_dim*1e3:273e-6:nRows*z_dim*1e3;
[VxGrid, VzGrid] = meshgrid (Vx,Vz);
Vq = interp2(xzGrid, zxGrid, thicknessY(:,:,1), VxGrid, VzGrid);
Vq = Vq./max(max(Vq));
Vq = flipud(Vq);
imwrite(Vq,'thickness_seg.png');

