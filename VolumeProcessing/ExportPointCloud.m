function ExportPointCloud( nodeList, folderPath )
%EXPORTPOINTCLOUD Summary of this function goes here
%   Detailed explanation goes here

for i = 1:length(nodeList)
        fileID = fopen(strcat(folderPath,'\',['point_cloud_t' sprintf('%04d',i) '.xyz']),'w');
        fprintf(fileID,'%1.6f %1.6f %1.6f \r\n',nodeList{i}');
        fclose(fileID);
end

