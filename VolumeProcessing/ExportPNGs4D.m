function [ output_args ] = ExportPNGs4D( V, folderPath )
%EXPORTPNGS Summary of this function goes here
%   Detailed explanation goes here

slices = size(V,3);
frames = size(V,4);

for frame = 1:frames
    for slice = 1:slices
        imwrite(mat2gray(V(:,:,slice,frame)',[0 255]),[folderPath sprintf('\\volume_t%04d_z%04d.png',frame,slice)])
    end
end

