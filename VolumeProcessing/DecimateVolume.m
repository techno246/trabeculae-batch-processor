function [ V_out ] = DecimateVolume( V, dec_factor, slices )
%DECIMATEVOLUME Summary of this function goes here
%   Detailed explanation goes here
    frames = size(V,3)/slices;
    frameIndex = 1;
    for i = 1:dec_factor:frames
        undecimatedRange = (i-1)*slices+1:i*slices;
        decimatedRange = (frameIndex-1)*slices+1:frameIndex*slices;
        V_out(:,:,decimatedRange) = V(:,:,undecimatedRange);
        frameIndex = frameIndex+1;
    end
end

