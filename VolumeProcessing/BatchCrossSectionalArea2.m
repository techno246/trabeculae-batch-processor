function [ areaAlongLength ] = BatchCrossSectionalArea2( V , slices, frames, x_dim, y_dim)
%BATCHMESH3D Summary of this function goes here
%   Returns 2D array of cross sectional area over time. Colum: pos, Row:
%   time

smoothing = true;
capEndArea = true;
capAmount = 5;

for n = 1:frames
    indexStart = (n-1)*slices+1;
    indexEnd = n*slices;

    V_bin = logical(V(:,:,indexStart:indexEnd));
    count = sum(sum(V_bin));
    area = reshape(count,size(count,3),1).*x_dim.*y_dim;
    
    if smoothing
        area = smooth(area,5);
    end
    
    
    if capEndArea 
        area = CapArea(area, capAmount);
    end
    
    areaAlongLength(n,:) = area;
    


end

end

