function [ V ] = ImportPNGs4D( folderPath, slices )
%IMPORTPNGS Imports 3D volume data from PNG dataset
%   Detailed explanation goes here

    contents = dir(folderPath);

    % Count Images
    nImages = 0;    
    for i = 1:length(contents)
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
            nImages = nImages+1;
        end
    end

    % Pre-initialize arrays
    % Note that dimensions are funky because iso2mesh processes arrays
    % where dimension 1 is x (rows)
    image = imread(strcat(folderPath,'\',contents(5).name))'; %Get size
    nRows = size(image,1);
    nColumns = size(image,2); 

    V = zeros(nRows,nColumns,slices,nImages/slices); 
   
    count = 0;

    % Iterate all files
    for i = 1:length(contents)
        % Only deal with images
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
            array = imread(strcat(folderPath,'\',contents(i).name))';
            slice = mod(count,slices)+1;
            frame = floor(count/slices)+1;
            V(:,:,slice,frame) = array;
            count = count+1; % increment
        end
    end
    
    V = V;

end

