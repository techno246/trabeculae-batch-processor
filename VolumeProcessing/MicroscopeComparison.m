%% PARAMETERS
imageDirectory = 'F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20151117 Passive\FirstSequence\StripeFiltered\Cropped\Segmented\ParticlesRemoved';
slices = 250;
frames = 8;
resampling_factor = 2;
x_dim = 2.7e-6/resampling_factor;
y_dim = 10e-6/resampling_factor;
z_dim = 10e-6;
FORCE_PRESENT = false;
stimFrequency = 1;

close all;

%% FIND AREA
if ~exist('areas', 'var')
    %% INTIALISE ARRAYS
    % Count Images
    contents = dir(imageDirectory);
    nImages = 0;    
    for i = 1:length(contents)
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
            nImages = nImages+1;
        end
    end

    % Pre-initialize arrays
    % Note that dimensions are funky because iso2mesh processes arrays
    % where dimension 1 is x (rows)
    image = imread(strcat(imageDirectory,'\',contents(5).name))'; %Get size
    nRows = size(image,1);
    nColumns = size(image,2); 

    areas = zeros(slices,nImages/slices); 

    count = 0;
    
    leftIndex = NaN(slices,frames);
    rightIndex = NaN(slices,frames);
    topIndex = NaN(slices,frames);
    bottomIndex = NaN(slices,frames);

    %% CALCULATE AREAS
    % Iterate all files
    for i = 1:length(contents)
        % Only deal with images
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
            array = imread(strcat(imageDirectory,'\',contents(i).name))';
            slice = mod(count,slices)+1;
            frame = floor(count/slices)+1;
            count = count+1; % increment

            % Do stuff
            areas(slice,frame) = sum(sum(logical(array))).*x_dim.*y_dim;
            
            % Count from left 
            for column = 1:1:nColumns
                if (sum(array(:,column))>0)
                    break;
                end
            end
            leftIndex(slice,frame) = column;
            
            % Count from right
            for column = nColumns:-1:1
                if (sum(array(:,column))>0)
                    break;
                end
            end
            rightIndex(slice,frame) = column;
            
            % Count from bottom 
            for row = 1:1:nRows
                if (sum(array(row,:))>0)
                    break;
                end
            end
            bottomIndex(slice,frame) = row;
            
            % Count from top
            for row = nRows:-1:1
                if (sum(array(row,:))>0)
                    break;
                end
            end
            topIndex(slice,frame) = row;
        end
    end
    
    heights = rightIndex-leftIndex;
    heights(heights<0) = NaN;
    widths = topIndex -bottomIndex;
    widths(widths<0) = NaN;
    
end

%% PLOT AREAS
smoothing = true;
colormap default;

zAxis = (1:slices)*z_dim*1e3; %en mm
time = ((1:frames)*(1/frames))./stimFrequency;

[TimeGrid, PosGrid] = meshgrid(time, zAxis);
%surf(TimeGrid', PosGrid', areaHold, 'LineStyle', 'none', 'FaceLighting', 'none', 'FaceColor', 'interp');
pcolor(TimeGrid', PosGrid', areas');
shading('interp');
%title(sprintf('Average cross-sectional area along muscle length during twitch'),'FontSize',14);
xlabel('Time (s)','FontSize',12); ylabel('Position along length (mm)','FontSize',12);
c = colorbar;
ylabel(c, 'Cross Sectional Area (m^2)','rot',-90,'FontSize',12);
caxis([1e-8  , 2e-8])

% Plot first row, and do comparison calculation
comparisonIndices = 50:223;
% What would've been calculated using a microscope approach
averageHeight = mean(heights(comparisonIndices,1));
averageWidth = mean(widths(comparisonIndices,1));
areaElliptical = averageWidth*x_dim/2*averageHeight*y_dim/2*pi;
areasElliptical(1:174) = areaElliptical;
volumeElliptical = areaElliptical*173*10e-6;
% OCT Estimation
volumeOCT = sum(areas(comparisonIndices,1))*10e-6;
% Plot the graphs
xaxis = (0:173)*10;
figure;
plot(xaxis,areas(50:223));
hold on;
plot(xaxis,areasElliptical);
xlabel('Position along length (\mum)','FontSize',12); ylabel({'Cross-sectional area (m^2)'},'FontSize',12);

    
%% OTHER SHIT
% Plot force

% if ~FORCE_PRESENT
%     subplot (3,1,1);
%     plot(ForceData(:,1),(ForceData(:,2)-min(ForceData(:,2)))*1e3);
%     xlabel('Time (s)','FontSize',12); ylabel({'Active Force (mN)'},'FontSize',12);
%     c = colorbar;
% end
% 
% % Find area
% subplot (3,1,2);
% [areas change] = BatchArea( areas , slices, frames_new, 0, stimFrequency, x_dim, y_dim, z_dim, dec_factor);
% 
% % Find stress
% if ~FORCE_PRESENT
%     subplot (3,1,3);
%     force_new = decimate(ForceData(:,2), dec_factor);
%     force_new = force_new - min(force_new);
%     [stress] = BatchStress(areas,force_new,stimFrequency);
% end
% 
% % Volume Fixed
% 
% % lengthEnd = 0.00147-0.00067;
% % lengthStart = 0.00163-0.00055;
% % lengths = linspace(lengthStart,lengthEnd,6);
% % volume2 = nanmean(areas,2)';
% % volume2=volume2.*lengths;
% % figure;
% % plot ([100,95,90,85,80,75], volume2);
% % title('Volume of muscle during stretch','FontSize',14);
% % xlabel('Muscle Length (%L_O)','FontSize',12); ylabel('Volume (m^3)','FontSize',12);
% % 
% % % Find volume
% % for frame = 1:frames_new
% %     TRI = faceList{frame}(:,1:3);
% %     X_ = nodeList{frame}(:,1);
% %     Y_ = nodeList{frame}(:,2);
% %     Z_ = nodeList{frame}(:,3);
% %     [volume(frame),surfaceArea(frame)] = triangulationVolume(TRI,X_,Y_,Z_);
% %     
% % end
% 
% %AnimateMesh( nodeList, faceList, frames, rate, dec_factor )
% 
% if FORCE_PRESENT
%     [ nodeListC, faceListC, cRange ] = ColorWithCSArea(nodeList, faceList, areas, slices, frames_new,  x_dim, y_dim, z_dim );
% else
%     decimatedForce = decimate(ForceData(:,2),dec_factor);
%     [ nodeListC, faceListC, cRange ] = ColorWithStress(nodeList, faceList, areas, slices, frames_new,  x_dim, y_dim, z_dim, decimatedForce );
%     %cRange = [0 9.68415013613380e-12]
% end
% 
% movieMatrix = AnimateMesh( nodeListC, faceListC, frames_new, rate, dec_factor_new, max(X), max(Y), max(Z)/frames, cRange );
% 
% 
