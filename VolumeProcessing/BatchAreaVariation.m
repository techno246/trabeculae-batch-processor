function [ ] = BatchAreaVariation( V , slices, frames, timeOffset, stimFrequency)
%BATCHMESH3D Summary of this function goes here
%   Detailed explanation goes here

x_dim = 10e-6;
y_dim = 2.7e-6*1.33;

figure;
% ColorSet = varycolor(frames);
% set(gca, 'ColorOrder', ColorSet);
hold all;
% legend off
% set(gcf, 'Colormap', ColorSet);
% colorbar
title('Variance in cross sectional area over length');
xlabel('Position (m)'); ylabel('Cross sectional area (m^2)') 

areaHold = cell(frames,1);


zAxis = (1:slices)*x_dim;
time = ((1:frames)*(1/frames))./stimFrequency;
lengthWiseMAD = zeros(1,frames);
lengthWiseMaxChange = zeros(1,frames);

for n = 1+timeOffset:timeOffset+frames
    indexStart = (n-1)*slices+1;
    indexEnd = n*slices;

    V_bin = logical(V(:,:,indexStart:indexEnd));
    count = sum(sum(V_bin));
    area = reshape(count,size(count,3),1).*x_dim.*y_dim;
    area = area;
    meanArea = mean(area)
    medianArea = median(area)
    averageArea(n) = meanArea;
    plot(zAxis,area);
    plot(zAxis,averageArea(n)*(ones(slices)));
    
    areaHold{n} = area;
    mad(area,1)
    lengthWiseMAD(n) = mad(area,1);
    plot(zAxis, averageArea(n)+mad(area,1)*(ones(slices)));
    plot(zAxis, averageArea(n)-mad(area,1)*(ones(slices)));
    
    lengthWiseMaxChange(n) = (meanArea-max(abs(max(area)),abs(min(area))))/meanArea*100;
end

hold off

figure; 
plot(time,averageArea)
title('Median cross sectional area during twitch');
xlabel('Time (s)'); ylabel('Median Cross Sectional Area (m^2)') 
hold on
plot (time, mean(averageArea)*(ones(frames)))
hold off


figure
plot (time, lengthWiseMAD)
title('Median absolute deviation of cross sectional area over time');
xlabel('Time (s)'); ylabel('MAD (m^2)') 

figure
plot (time, lengthWiseMAD./(averageArea*100))
title('Median absolute deviation of cross sectional area over time');
xlabel('Time (s)'); ylabel('MAD (%)') 

figure
plot (time, lengthWiseMaxChange)
title('Maximum variation of cross sectional area along length');
xlabel('Time (s)'); ylabel('Maximum variation (%)') 


end

