function [ stress ] = BatchStress( area , force , stimFrequency)
%BATCHMESH3D Summary of this function goes here
%   Detailed explanation goes here

slices = size(area,2);
frames = size(area,1);
x_dim = 10e-6/2;
z_dim = 10e-6;

zAxis = (1:slices)*z_dim*1e3;   % in mm
time = ((1:frames)*(1/frames))./stimFrequency;



for y = 1:slices
    stress(:,y) = force./area(:,y)./1000;
    %Convert to kPa
end



[TimeGrid, PosGrid] = meshgrid(time, zAxis);
%surf(TimeGrid', PosGrid', stress, 'LineStyle', 'none', 'FaceLighting', 'none', 'FaceColor', 'interp');

%hm = figure;
pcolor(TimeGrid', PosGrid', stress);
shading('interp');
%title('Average stress along muscle length during twitch','FontSize',14);
xlabel('Time (s)','FontSize',12); ylabel({'Position along length (mm)'},'FontSize',12); 
c = colorbar;
ylabel(c, 'Active Stress (kPa)','rot',-90,'FontSize',12);
end

