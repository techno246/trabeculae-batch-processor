function [ areaHold area_change ] = BatchArea( V , slices, frames, timeOffset, stimFrequency, x_dim, y_dim, z_dim, dec_factor)
%BATCHMESH3D Summary of this function goes here
%   Detailed explanation goes here

smoothing = true;
colormap default;
areaHold = zeros(frames,slices);

zAxis = (1:slices)*z_dim*1e3; %en mm
time = ((1:frames)*(1/frames))./stimFrequency;

for t = 1+timeOffset:timeOffset+frames
    indexStart = (t-1)*slices+1;
    indexEnd = t*slices;

    V_bin = logical(V(:,:,indexStart:indexEnd));
    count = sum(sum(V_bin));
    area = reshape(count,size(count,3),1).*x_dim.*y_dim;
    if smoothing
        area = smooth(area,5);
    end
    
    meanArea = mean(area);

    %NaN on zero values 
    area(area == 0) = NaN;
    area=CapAreaDestructive(area,3);
    
    areaHold(t,:) = area;
    
end

for i = 1:frames
    area_change(i,:) = areaHold(i,:)./areaHold(1,:);
end

area_change = (area_change-1)*100;

%figure;

[TimeGrid, PosGrid] = meshgrid(time, zAxis);
%surf(TimeGrid', PosGrid', areaHold, 'LineStyle', 'none', 'FaceLighting', 'none', 'FaceColor', 'interp');
pcolor(TimeGrid', PosGrid', areaHold);
shading('interp');
%title(sprintf('Average cross-sectional area along muscle length during twitch'),'FontSize',14);
xlabel('Time (s)','FontSize',12); ylabel('Position along length (mm)','FontSize',12);
c = colorbar;
ylabel(c, 'Cross Sectional Area (m^2)','rot',-90,'FontSize',12);

end

