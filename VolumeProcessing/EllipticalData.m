clear;

% Parameters
x_dim = 2.7e-6/2;
y_dim = 2.7e-6/2;

% Read in measurements as table
measurements = readtable('F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20160908\OCT\Data\EllipticalAnalysis.csv');

% Get table parameters
nRows = size(measurements,1);
nColumns = size(measurements,2);
nFrames = max(measurements.Frame);
nSlices = max(measurements.Slice);

% Setup variables
Major = NaN(nSlices,nFrames);
Minor = NaN(nSlices,nFrames);
Angle = NaN(nSlices,nFrames);
XDiameter = NaN(nSlices,nFrames);
YDiameter = NaN(nSlices,nFrames);
AreaUnscaled = NaN(nSlices,nFrames);

% Iterate table and fill variables with parameters, and converting to real
% units
for row = 1:nRows
    frame = measurements.Frame(row);
    slice = measurements.Slice(row);
    area_unscaled = measurements.Area(row);
    
    if isnan(AreaUnscaled(slice,frame)) || area_unscaled > AreaUnscaled(slice,frame)
        AreaUnscaled(slice,frame) = area_unscaled;
        
        major_unscaled = measurements.Major(row);
        minor_unscaled = measurements.Minor(row);
        angle = measurements.Angle(row);
        Angle(slice,frame) = angle;
        
        %Components Major Axis
        xcomp = major_unscaled*cosd(angle);
        ycomp = major_unscaled*sind(angle);
        Major(slice,frame) = sqrt((xcomp*x_dim)^2 + (ycomp*y_dim)^2);
        XDiameter(slice,frame) = abs(xcomp*x_dim);
        
        %Components Minor Axis
        xcomp = minor_unscaled*sind(angle);
        ycomp = minor_unscaled*cosd(angle);
        Minor(slice,frame) = sqrt((xcomp*x_dim)^2 + (ycomp*y_dim)^2);
        YDiameter(slice,frame) = abs(ycomp*y_dim);
    end
    
    AreaEllipse = pi.*(Major./2).*(Minor./2);
end
