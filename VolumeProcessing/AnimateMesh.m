function [ movieMatrix ] = AnimateMesh( nodeList, faceList, frames, rate, dec_factor, xRange, yRange, zRange, cRange )
%ANIMATEMESH Summary of this function goes here
%   Detailed explanation goes here

    % Please set these to the appropriate parameters for the current image
    SCALE_EACH_FRAME = false;

    handle = figure;
    axis off
    axis manual

    if ~SCALE_EACH_FRAME 
        caxis manual
        caxis([cRange(1),cRange(2)])
    end

    set(handle,'Renderer','ZBuffer');
%     winsize = get(handle,'Position');
%     winsize(1:2) = [0 0];
%     set(handle,'NextPlot','replacechildren')
    movieMatrix = moviein(frames,handle);

    
    % Flag
    firstFigure = true;
    saveOutput = false;

    % Animate display
    for i = 1:100
        for n = 1:frames
            tic;
            if mod(n,dec_factor) == 0
                figure(handle)
                hm = plotmesh(nodeList{n},faceList{n},'facecolor','interp','linestyle','-');
                xlim([0 xRange]); ylim([0 yRange]); zlim([0 zRange]); 
                daspect([1 1 1])
            end
            
            if firstFigure
                userInput = input('Please select from the following options. [1] = animate & save, [2] = animate, [3] = quit\n');
                if userInput == 1
                    saveOutput = true;
                elseif userInput == 2
                    
                else 
                    break;
                end
                firstFigure = false;
            end
            
            if saveOutput 
                movieMatrix(:,n) = getframe(handle);
            end
            
            pause(1/rate-toc);
        end
        
        if userInput~=2
            if userInput == 1
                movie2avi(movieMatrix,'MovieOutput.avi', 'fps', rate);
            end
            
            break;
        end
    end

end

