function [ nodeList, faceList ] = BatchMesh3D( V , slices, frames )
%BATCHMESH3D Summary of this function goes here
%   Detailed explanation goes here

nodeList = cell(frames,1);
faceList = cell(frames,1);

for n = 1:frames
    indexStart = (n-1)*slices+1;
    indexEnd = n*slices;
    [node,face] = v2s(V(:,:,indexStart:indexEnd),0.5,5,'cgalmesh');
    [node,face] = meshcheckrepair(node,face,'meshfix');
    [node,face] = meshresample(node,face,0.4);
    node=sms(node,face,5,0.1);
    nodeList{n} = node;
    faceList{n} = face;
end

end

