function [ area ] = CapArea( area, capAmount )
%CAPAREA Replaces end values of area due to mesh smoothing with the
%neighbouring values
%   Detailed explanation goes here

    pos = 1;
    % Upwards
    while area(pos) == 0 || isnan(area(pos))
        pos = pos+1;
        if pos > length(area)
            break
        end
    end
    if pos <= length(area)
        area(pos:pos+capAmount) = area(pos+capAmount+1);
    end

    %Downwards
    pos = length(area);

    while area(pos) == 0 || isnan(area(pos))
        pos = pos-1;
        if pos < 0
            break
        end
    end
    if pos >= 0 
        area(pos-capAmount:pos) = area(pos-capAmount-1);
    end
end

