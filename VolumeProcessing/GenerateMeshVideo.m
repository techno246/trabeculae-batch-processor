function [  ] = AnimateMesh( nodeList, faceList, frames, rate, dec_factor, xRange, yRange, zRange, cRange )
%ANIMATEMESH Summary of this function goes here
%   Detailed explanation goes here

    % Please set these to the appropriate parameters for the current image
    SCALE_EACH_FRAME = false;

    close all
    figure
    axis off
    axis manual

    if ~SCALE_EACH_FRAME 
        caxis manual
        caxis([cRange(1),cRange(2)])
    end

    % Find limits
    
    % Animate display
    for i = 1:100
        for n = 1:frames
            tic;
            if mod(n,dec_factor) == 0
                hm = plotmesh(nodeList{n},faceList{n},'facecolor','interp','linestyle','-');
                xlim([0 xRange]); ylim([0 yRange]); zlim([0 zRange]); 
                daspect([1 1 1])
            end

            pause(1/rate-toc);
        end
    end

end

