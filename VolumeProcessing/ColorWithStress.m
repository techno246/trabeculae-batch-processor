function [ nodeList, elemList, range ] = ColorWithStress(  nodeList, elemList, V, slices, frames, x_dim, y_dim, z_dim, force )
%COLORWITHCSAREA Summary of this function goes here
%   Detailed explanation goes here

[ area ] = BatchCrossSectionalArea2( V , slices, frames, x_dim, y_dim);
area(area == 0) = NaN;
stress = BatchStress2(area,force);

minStress=min(min(stress))
maxStress=max(max(stress))
disp('range')
rangeTotal = maxStress-minStress
range = [minStress maxStress];

for i = 1:frames
    dim = size(nodeList{i},1);
    frameArea = stress(i,:);
    zPositions = nodeList{i}(:,3);
    rangeZ = z_dim*slices;
    zPositions = ((zPositions)/rangeZ)*(slices-1) + 1;
    interpolated = interp1(frameArea,zPositions);
    %interpolated = 100*(interpolated-minArea)/rangeTotal;
    %interpolated=interpolated.*1e8;
    %interpolated = round(interpolated);
    nodeList{i}(:,4) = interpolated;
end

    
end

