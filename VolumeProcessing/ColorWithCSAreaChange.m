function [ nodeList, elemList, range ] = ColorWithCSAreaChange(  nodeList, elemList, V, slices, frames, x_dim, y_dim, z_dim )
%COLORWITHCSAREA Summary of this function goes here
%   Detailed explanation goes here

[ area ] = BatchCrossSectionalArea2( V , slices, frames, x_dim, y_dim);
area(area == 0) = NaN;
for i = 1:frames
    area_change(i,:) = area(i,:)./area(1,:);
end

%Convert to percent
area_change = (area_change- 1).*100

minArea=min(min(area_change))
maxArea=max(max(area_change))
range = [minArea maxArea];
disp('range')
rangeTotal = maxArea-minArea
    
for i = 1:frames
    dim = size(nodeList{i},1);
    frameArea = area_change(i,:);
    zPositions = nodeList{i}(:,3);
    rangeZ = z_dim*slices;
    zPositions = ((zPositions)/rangeZ)*(slices-1) + 1;
    interpolated = interp1(frameArea,zPositions);
    %interpolated = 100*(interpolated-minArea)/rangeTotal;
    %interpolated=interpolated.*1e8;
    %interpolated = round(interpolated);
    nodeList{i}(:,4) = interpolated;
end

    
end

