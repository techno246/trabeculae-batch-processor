function [ nodeList ] = ImportOFFs( folderPath )
%IMPORTPNGS Imports 3D volume data from PNG dataset
%   Detailed explanation goes here

    contents = dir(folderPath);

    % Count Images
    nImages = 0;    
    for i = 1:length(contents)
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'off'))
            nImages = nImages+1;
        end
    end

    % Pre-initialize arrays
    nodeList = cell(nImages,2);
   
    %Z = linspace(0,(nImages+2-1)*zDim,nImages+2);
    %V = zeros(nRows,nColumns,nImages+2);   %Pad top and bottom with zeros

    count = 0;

    % Iterate all files
    for i = 1:length(contents)
        % Only deal with images
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'off'))
            count = count+1;
            [nodeList{count,1}, nodeList{count,2}] = readoff(strcat(folderPath,'\',contents(i).name));
        end
    end
end

