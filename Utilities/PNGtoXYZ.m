function [ XYZ ] = PNGtoXYZ( folderPath, nFrames, xDim, yDim, zDim )
%IMPORTPNGS Imports 3D volume data from PNG dataset and exports as XYZ
%point cloud
%   Detailed explanation goes here
%   [XYZ] = PNGtoXYZ('D:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20140316\Trabecula 16_03_2014 c (readjusted)\Volume', 3.17, 6.7, 6.7);

    contents = dir(folderPath);

    % Count Images & purge everything else
    nImages = 0;    
    for i = 1:length(contents)
        if ~contents(i).isdir && ~isempty(strfind(contents(i).name,'png'))
            nImages = nImages+1;  
            imagePaths{nImages} = strcat(folderPath,'\',contents(i).name);
        end
    end

    % Pre-initialize arrays
    image = imread(strcat(folderPath,'\',contents(5).name)); %Get size
    nRows = size(image,1);
    nColumns = size(image,2); 

    X = linspace(0,(nColumns-1)*xDim,nColumns);
    Y = linspace(0,(nRows-1)*yDim,nRows);
    Z = linspace(0,(nImages+2-1)*zDim,nImages+2);
    XYZ = zeros(1000000,3);
    
    frameLength = nImages/nFrames;

    for t = 1:nFrames
        pointCount = 0;
        for z = 1:frameLength
            % Only deal with images
            array = imread(imagePaths{(t-1)*frameLength + z});
            for y = 1:nRows
                for x = 1:nColumns
                    %look for black pixels
                    if array(y,x)<128 
                        pointCount = pointCount+1;
                        XYZ(pointCount, :) = [X(x), Y(y), Z(z)];
                    end
                end
            end
        end 

        fileID = fopen(strcat(folderPath,'\',['point_cloud_t' sprintf('%04d',t) '.xyz']),'w');
        fprintf(fileID,'%1.6f %1.6f %1.6f \r\n',XYZ(1:pointCount,:)');
        fclose(fileID);
    end
end

