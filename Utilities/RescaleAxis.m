function [ h_new ] = RescaleAxis( figureHandle, axis, scaleFactor, newLabel )
%RESCALEAXIS Summary of this function goes here
%   Detailed explanation goes here

axesObjs = get(figureHandle, 'Children');
%axesObjs = axesObjs(2);
dataObjs = get(axesObjs, 'Children'); %handles to low-level graphics objects in axes


if axis == 'x'
    axisName = 'XData';  
    limitsName = 'XLim';
    xlabel(newLabel,'FontSize',12);
elseif axis == 'y'
    axisName = 'YData'; 
    limitsName = 'YLim';
    ylabel(newLabel,'FontSize',12);
elseif axis == 'z'
    axisName = 'ZData'; 
    limitsName = 'ZLim';
    zlabel(newLabel,'FontSize',12);
else
    axisName = ''; 
end

if ~strcmp(axisName,'')
    % Scale data
    data = get(dataObjs, {axisName}); %data from low-level grahics objects
    for i = 1:length(data)
        data{i} = data{i}.*scaleFactor;
    end
    set(dataObjs, {axisName}, data)
    
    % Scale limits
    currentLimits = get(axesObjs,limitsName);
    set(axesObjs,limitsName,currentLimits+scaleFactor);

    
else
    disp('Input error');
end

end

