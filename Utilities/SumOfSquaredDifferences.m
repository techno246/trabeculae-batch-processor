slices = 3;
frames = 1;

% Import images
disp('Importing images...')
[ V1 ] = ImportPNGs4D( 'F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20150124\Trabec 2\1Hz10ms_All_N0\CroppedFiltered\Classified\t083', slices );
[ V2 ] = ImportPNGs4D( 'F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20150124\Trabec 2\1Hz10ms_All_N0\CroppedFiltered\Classified\t084', slices );

rows = size(V1,1);
columns = size(V1,2);

% Convert to 4D 
%V1 = reshape(V1,size(V1,1),size(V1,2),size(V1,3)/frames,size(V1,3)/slices);
%V2 = reshape(V2,size(V2,1),size(V2,2),size(V2,3)/frames,size(V2,3)/slices);

sumSquareDiff = zeros(slices, frames);

% disp('Filtering...')
% for i = 1:rows
%     disp(i)
%     for j = 1:columns
%         for k = 1:slices
%              temp = reshape(V1(i,j,k,:),frames,1);
%              V1(i,j,k,:) = reshape(sgolayfilt(temp,3,33),1,1,1,frames);
%              temp = reshape(V2(i,j,k,:),frames,1);
%              V2(i,j,k,:) = reshape(sgolayfilt(temp,3,33),1,1,1,frames);
%         end
%     end
% end

% Find sum squared diff over two dimensions (slice and frame)

%%%%%%%%%%%%%%%%%% FOR BINARY IMAGES %%%%%%%%%%%%%%%%%%%
V1 = logical (V1);
V2 = logical (V2);

%%%%%%%%%%%%%%%%%% TWO IMAGES %%%%%%%%%%%%%%%%%%%
disp('Subtracting...')
for k = 1:slices
    for t = 1:frames
        %sumSquareDiff(k,t) = sqrt(sumsqr(V2(:,:,k,t)-V1(:,:,k,t)));
        sumSquareDiff(k,t) = sum(sum(imabsdiff(V2(:,:,k,t),V1(:,:,k,t)))); % sum absolute difference
    end
end

zAxis = (1:slices)*10e-6;
time = ((1:frames)*(1/frames))./1;

figure;
[TimeGrid, PosGrid] = meshgrid(time, zAxis);
pcolor(TimeGrid, PosGrid, sumSquareDiff);
shading('interp');
title(sprintf('Average cross-sectional area along muscle length during twitch'),'FontSize',14);
xlabel('Time (s)','FontSize',12); ylabel('Position (m)','FontSize',12);


%%%%%%%%%%%%%%%%%% SHIFTED CORRELATION %%%%%%%%%%%%%%%%
% V1_shifted = circshift(V1,[0 0 0 -50]);
% sumSquareDiffFramesShifted = zeros(frames,1);
% sumSquareDiffTotal = zeros(100,1);
% for i = 1:101 
%     for t = 1:frames
%         sumSquareDiffFramesShifted(t) = sqrt(sumsqr(V2(:,:,:,t)-V1_shifted(:,:,:,t)));
%     end
%     sumSquareDiffTotal(i) = sum(sumSquareDiffFramesShifted);
%     V1_shifted = circshift(V1_shifted,[0 0 0 1]);
% end
% 
% phase_shift = linspace(-180,180,101);
% 
% figure;
% plot(phase_shift,sumSquareDiffTotal);
% title(sprintf('Sum squared difference with time phase shift applied'),'FontSize',14);
% xlabel('Phase shift (\circ)','FontSize',12); ylabel('Sum Squared Difference','FontSize',12);



%%%%%%%%%%%%%%%%%% PLOTTING DISCRETE POINTS %%%%%%%%%%%%%%%%


% 
% % Find shifted sum squared diff against frame 1 (slice and frame)
% disp('Subtracting...')
% for k = 1:slices
%     for t = 1:frames
%         sumSquareDiffInitial(k,t) = sqrt(sumsqr(V1(:,:,k,t)-V1(:,:,k,1)));
%     end
% end
% 
% figure;
% [TimeGrid, PosGrid] = meshgrid(time, zAxis);
% pcolor(TimeGrid, PosGrid, sumSquareDiffInitial);
% shading('interp');
% title(sprintf('Average cross-sectional area along muscle length during twitch'),'FontSize',14);
% xlabel('Time (s)','FontSize',12); ylabel('Position (m)','FontSize',12);
% 
% % Find sum squared diff over one dimension (frame)
% for t = 1:frames
%     sumSquareDiffFrames(t) = sqrt(sumsqr(V2(:,:,:,t)-V1(:,:,:,t)));
% end
% % Find shifted sum squared diff over one dimension (frame)
% for t = 1:frames
%     sumSquareDiffFramesInitial(t) = sqrt(sumsqr(V1(:,:,:,t)-V1(:,:,:,1)));
% end
% 
% V1_shifted(:,:,:,51:100) = V1(:,:,:,1:50);
% V1_shifted(:,:,:,1:50) = V1(:,:,:,51:100);
% % Find shifted sum squared diff over one dimension (frame)
% for t = 1:frames
%     sumSquareDiffFramesShifted180(t) = sqrt(sumsqr(V2(:,:,:,t)-V1_shifted(:,:,:,t)));
% end
% V1_shifted(:,:,:,26:100) = V1(:,:,:,1:75);
% V1_shifted(:,:,:,1:25) = V1(:,:,:,76:100);
% for t = 1:frames
%     sumSquareDiffFramesShifted90(t) = sqrt(sumsqr(V2(:,:,:,t)-V1_shifted(:,:,:,t)));
% end
% V1_shifted(:,:,:,13:100) = V1(:,:,:,1:88);
% V1_shifted(:,:,:,1:12) = V1(:,:,:,89:100);
% for t = 1:frames
%     sumSquareDiffFramesShifted45(t) = sqrt(sumsqr(V2(:,:,:,t)-V1_shifted(:,:,:,t)));
% end
% 
% figure;
% hold all;
% plot(time,sumSquareDiffFrames);
% plot(time,sumSquareDiffFramesShifted45);
% plot(time,sumSquareDiffFramesShifted90);
% plot(time,sumSquareDiffFramesShifted180);
% title(sprintf('Sum squared difference of muscle between first and second volume scan'),'FontSize',14);
% xlabel('Time (s)','FontSize',12); ylabel('Sum Squared Difference','FontSize',12);
% legend('0\circ shift','45\circ shift','90\circ shift','180\circ shift');

%%%%%%%%%%%%%%%%%%%%%%% END OF USEFUL STUFF %%%%%%%%%%%%%%%%%%%%%%%%%%  

% Extra shit

% Time average
% for n = 1:2:frames
%     indexStart1 = (n-1)*slices+1;
%     indexEnd1 = n*slices;
%     indexStart2 = (n)*slices+1;
%     indexEnd2 = (n+1)*slices;
%     
%     indexStartResult = ((n+1)/2-1)*slices+1;
%     indexEndResult = (n+1)/2*slices;
%     
%     V_diff(:,:,indexStartResult:indexEndResult) = (V1(:,:,indexStart2:indexEnd2)+V1(:,:,indexStart1:indexEnd1))./2;
% end


% 
% % Find shifted sum squared diff over two dimensions (slice and frame)
% disp('Subtracting...')
% for k = 1:slices
%     for t = 1:frames
%         sumSquareDiffShifted(k,t) = sqrt(sumsqr(V2(:,:,k,t)-V1_shifted180(:,:,k,t)));
%     end
% end
% 
% figure;
% [TimeGrid, PosGrid] = meshgrid(time, zAxis);
% pcolor(TimeGrid, PosGrid, sumSquareDiffShifted);
% shading('interp');
% title(sprintf('Average cross-sectional area along muscle length during twitch'),'FontSize',14);
% xlabel('Time (s)','FontSize',12); ylabel('Position (m)','FontSize',12);