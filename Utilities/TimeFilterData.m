slices = 101;
frames = 100;

% Import images
disp('Importing images...')
[ V1 ] = ImportPNGs4D( 'F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20150124\Trabec 2\1Hz10ms_All_N0\Renamed', slices );

% Get dims 
rows = size(V1,1);
columns = size(V1,2);


%Filtering
disp('Filtering...')
for i = 1:rows
    disp(i)
    for j = 1:columns
        for k = 1:slices
             temp = reshape(V1(i,j,k,:),frames,1);
             V1(i,j,k,:) = reshape(sgolayfilt(temp,3,11),1,1,1,frames);
        end
    end
end

disp('Exporting...')
ExportPNGs4D( V1, 'F:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20150124\Trabec 2\1Hz10ms_All_N0\TimeFiltered' )
