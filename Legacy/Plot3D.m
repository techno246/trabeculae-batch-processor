[X, Y, Z, V] = ImportPNGs('D:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20140316\Trabecula 16_03_2014 c (readjusted)\Volume', 3.17, 6.73, 6.73);

V = smooth3(V, 'box',3);
fv = isosurface(X, Y, Z, V, 128);
p = patch(fv);
isonormals(X, Y, Z, V, p);
set(p,'FaceColor','red','EdgeColor','none');
daspect([1,1,1])
view(3); axis tight
camlight 
lighting gouraud 
%{
%}