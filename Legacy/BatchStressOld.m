function [ stress ] = BatchStressOld( area , force , stimFrequency)
%BATCHMESH3D Summary of this function goes here
%   Detailed explanation goes here

dec_factor = 4;
slices = size(area,2);
frames = size(area,1);
x_dim = 10e-6;


time = ((1:frames)*(1/frames))./stimFrequency;
decimatedForce = decimate(force(:,2),1);

for y = 1:slices
    stress(:,y) = decimatedForce./area(:,y);
end

figure;
hold all;
cm=colormap(jet(frames/dec_factor));
caxis([0 slices*x_dim]) ;
cm_index = 1;
for i = 1:dec_factor:frames
     plot(time,stress(:,i),'color',cm(cm_index,:));
     cm_index = cm_index + 1;
end
title('Stress during contraction at different cross sections','FontSize',14);
xlabel('Time (s)','FontSize',12); ylabel({'Stress (Pa)'},'FontSize',12);

figure;
stressHigh = mean(stress(:,39:42),2);
stressLow = mean(stress(:,98:101),2);
averageStress = (stressHigh+stressLow)./2;
diffStress = (stressHigh-stressLow);
variation = diffStress./averageStress;
disp('mean');
mean(variation)
plot (time,variation.*100);
title('Variation of stress over time','FontSize',14);
xlabel('Time (s)','FontSize',12); ylabel({'Stress Range (%)'},'FontSize',12);

end

