[X, Y, Z, V] = ImportPNGs('D:\Ming Cheuk\Documents\Cardiac Myometer\OCT\Data\20140717 Full Scan\Export 4 (Time = 16 Averaged, FIXED)\Processed_ZStack_FutherRemoval\Segmented 20140916', 3.17, 6.73, 6.73);

V = cast(V,'uint8');
%V = deislands3d(V, 1);
%V = fillholes3d(V, 2);

% %% create 3D mesh          |--------> threshold at v=0.7
% [node,elem,face]=v2m(V,128,5,10000, 'cgalmesh');
% %                             |  |-> maximum volume
% %                             |----> maximum surface triangle size
% 
% figure;
% 
% plotmesh(node,face);axis equal;view(90,60);


[node,face]=v2s(V,0.5,5,'cgalmesh');
[n,e] = meshresample(node,face,0.2);
newnode=sms(n,e,10,.1);
[node,elem,face] = s2m(n,e,1,1000);
%node=node(:,1:3);
%face=face(:,1:3);

figure;
plotmesh(newnode,e);
figure;
% 
% plotmesh(node,face);
% figure;
% plotmesh(node,elem);
% axis equal