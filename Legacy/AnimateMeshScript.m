% Please set these to the appropriate parameters for the current image
slices = 220;
frames = 5;
x_dim = 10e-6;
y_dim = 2.7e-6*1.33;
COLOR_WITH_AREA = true;
COLOR_WITH_STRESS = false;
SCALE_EACH_FRAME = false;
rate = 5; %rate in fps
dec_factor = 1; %decimate display

    for n = 1:frames
        %[nodeList{n,1}, elem, nodeList{n,2}] = s2m(nodeList{n,1},nodeList{n,2}, 1, 1);
    end
close all
figure
axis off
axis manual

%daspect([2.7 7.4 2.7])

if COLOR_WITH_AREA
    [ nodeListC, faceListC, range ] = ColorWithCSAreaChange(nodeList, faceList, V, slices, frames,  x_dim, y_dim );
elseif COLOR_WITH_STRESS
    [ nodeListC, faceListC, range ] = ColorWithStress(nodeList, faceList, V, slices, frames,  x_dim, y_dim, ForceData(:,2) );
end

if ~SCALE_EACH_FRAME 
    caxis manual
    caxis([range(1),range(2)])
end


% Animate display
for i = 1:100
    for n = 1:frames
        tic;
        if mod(n,dec_factor) == 0
            if COLOR_WITH_AREA || COLOR_WITH_STRESS
                plotmesh(nodeListC{n},faceListC{n},'facecolor','interp','linestyle','-');
            else
                plotmesh(nodeListC{n},faceListC{n},'facecolor','red','linestyle','-');
            end
            daspect([2.7 7.4 2.7])
        end
        timeDiff = toc;
        pause(1/rate-toc);
    end
end
