function [ areaHold area_change ] = BatchAreaOld( V , slices, frames, timeOffset, stimFrequency, x_dim, y_dim, z_dim, dec_factor)
%BATCHMESH3D Summary of this function goes here
%   Detailed explanation goes here

smoothing = true;

figure;
% ColorSet = varycolor(frames);
% set(gca, 'ColorOrder', ColorSet);
hold all;
% legend off
% set(gcf, 'Colormap', ColorSet);
% colorbar

title(sprintf('Cross sectional area over length of muscle\n plotted at different instances in time'),'FontSize',14);
xlabel('Position (m)','FontSize',12); ylabel('Cross sectional area (m^2)','FontSize',12);

areaHold = zeros(frames,slices);

cm=colormap(jet(frames));
%cm=colormap(zeros(frames,3));

zAxis = (1:slices)*z_dim;
time = ((1:frames)*(1/frames))./stimFrequency;
lengthWiseMAD = zeros(1,frames);
lengthWiseMaxChange = zeros(1,frames);

caxis([min(time) max(time)]) 

% V = logical(V);
% for i = 1:slices*frames 
%     V(:,:,i) = deislands2d(V(:,:,i),1000);
% end

for t = 1+timeOffset:timeOffset+frames
    indexStart = (t-1)*slices+1;
    indexEnd = t*slices;

    V_bin = logical(V(:,:,indexStart:indexEnd));
    count = sum(sum(V_bin));
    area = reshape(count,size(count,3),1).*x_dim.*y_dim;
    if smoothing
        area = smooth(area,5);
    end
    
    %NaN on zero values 
    area(area == 0) = NaN;
    area=CapAreaDestructive(area,3);
    if mod(t,dec_factor) == 0
        plot(zAxis,area,'color',cm(t,:));
    end
    %plot(zAxis,averageArea(t)*(ones(slices)),'color',cm(t,:));
    
    meanArea = mean(area);
    medianArea = median(area);
    averageArea(t) = meanArea;
    
    areaHold(t,:) = area;
    
    lengthWiseMaxChange(t) = (meanArea-max(abs(max(area)),abs(min(area))))/meanArea*100;
end


figure; 
hold all;

cm=colormap(jet(slices));
for y = 1:slices
    plot (time, areaHold(:,y),'color',cm(y,:));
end

for i = 1:frames
    area_change(i,:) = areaHold(i,:)./areaHold(1,:);
end

figure; 
area_change = (area_change-1)*100;
plot (zAxis, mean(area_change(2:3,:)));
title(sprintf('Change in cross sectional area along\nlength of muscle at peak force'),'FontSize',14);
xlabel('Position (\m)','FontSize',12); ylabel({'Change in','Cross Sectional Area (%)'},'FontSize',12);

figure;
plot (time, mean(area_change'));
title('Mean change in cross sectional area during twitch');
xlabel('Time (s)'); ylabel({'Mean Change in','Cross Sectional Area (%)'});

figure; 
plot(time,averageArea)
title(sprintf('Mean cross sectional area during twitch'),'FontSize',14);
xlabel('Time (s)'); ylabel('Mean Cross Sectional Area (\m^2)') 

figure
plot (time, lengthWiseMaxChange)
title('Maximum variation of cross sectional area along length');
xlabel('Time (s)'); ylabel('Maximum variation (%)') 


end

